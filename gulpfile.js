var gulp           = require('gulp');
var sass           = require('gulp-sass');
var useref         = require('gulp-useref');
var gulpIf         = require('gulp-if');
var cache          = require('gulp-cache');
var uglify         = require('gulp-uglify');
var imagemin       = require('gulp-imagemin');
var cssnano        = require('gulp-cssnano');
var plumber        = require('gulp-plumber');
var autoprefixer   = require('gulp-autoprefixer');
var sourcemaps     = require('gulp-sourcemaps');
var inject         = require('gulp-inject');
var htmlhint       = require('gulp-htmlhint');
var csslint        = require('gulp-csslint');
var notify         = require('gulp-notify');
var bowerFiles     = require('main-bower-files');
var eventStream    = require('event-stream');
var del            = require('del');
var runSequence    = require('run-sequence');
var browserSync    = require('browser-sync').create();

var basePaths = {
  src: 'src/',
  dest: 'dist/',
  bower: 'bower_components/'
};

// Error handling
var onError = function(err) {
    notify.onError({
      title:    "Gulp error in " + err.plugin,
      message:  err.toString()
    })(err);
    this.emit('end');
};

// sass
gulp.task('sass', function(){
  return gulp.src(basePaths.src + '/assets/css/**/*.scss')
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(sass())
      .pipe(autoprefixer({
        browsers: ['last 2 versions']
      }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(basePaths.src + '/assets/css'))
    .pipe(csslint())
    .pipe(browserSync.reload({
      stream: true
    }))
});


// Minify and copy images
gulp.task('images', function(){
  return gulp.src(basePaths.src + '/assets/img/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(
      imagemin()
    ))
    .pipe(gulp.dest(basePaths.dest + '/assets/img'))
});

// Copy fonts to dist
gulp.task('fonts', function(){
  return gulp.src(basePaths.src + '/assets/fonts/**/*')
    .pipe(gulp.dest(basePaths.dest + '/assets/fonts'))
});


// HTML lint
gulp.task('htmlhint', function(){
  gulp.src(basePaths.src + '*.html')
    .pipe(htmlhint())
    .pipe(htmlhint.reporter())
});

// Inject css/js
gulp.task('inject', function() {
 gulp.src([basePaths.src + '/**/*.html', basePaths.src + '/**/*.php'])
  .pipe(inject(gulp.src(bowerFiles({includeDev:true}), {read: false, cwd: __dirname + '/src'}), {name: 'bower'}))
  .pipe(inject(eventStream.merge(
    gulp.src('./src/assets/**/*.css'),
    gulp.src('./src/assets/**/*.js', {read: false})
  ), {ignorePath: basePaths.src, addRootSlash: false}))
  .pipe(gulp.dest(basePaths.src));
 });


 // Concat css/js with useref, copy files over
 gulp.task('useref', function(){
   return gulp.src([basePaths.src + '/**/*.html', basePaths.src + '/**/*.php'])
     .pipe(useref())
     .pipe(gulpIf('*.css', cssnano()))
     .pipe(gulpIf('*.js', uglify()))
     .pipe(gulp.dest(basePaths.dest))
 });


// browserSync
gulp.task('browserSync', function(){
  browserSync.init({
    notify: false,
    port: 3000,
    server: {
      baseDir: basePaths.src,
      routes: {
        '/bower_components': basePaths.bower,
      },
    }
  });
});

// PHP? needs testing
// gulp.task('browserSync', function(){
//   browserSync.init({
//       proxy: 'localhost:8888/YOUR_DIR_NAME/' + basePaths.src,
//       routes: {
//         '/bower_components': basePaths.bower,
//     }
//   });
// });


// Clean dist folder
gulp.task('clean:dist', function(){
  return del.sync(basePaths.dest);
});


// Watch
gulp.task('watch', ['browserSync', 'sass'], function(){
  gulp.watch(basePaths.src + '/assets/css/**/*.scss', ['sass']);
  gulp.watch(basePaths.src + '/**/*.php', browserSync.reload);
  gulp.watch(basePaths.src + '/**/*.html', browserSync.reload);
  gulp.watch(basePaths.src + '/**/*.js', browserSync.reload);
});



// Build
gulp.task('build', function(callback){
  runSequence('clean:dist',
    ['inject', 'sass', 'useref', 'images', 'fonts'],
    callback
  )
});

// Default
gulp.task('default', function(callback){
  runSequence(['htmlhint', 'sass', 'browserSync', 'watch'],
    callback
  )
})
